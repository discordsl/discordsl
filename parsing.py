from rply.token import BaseBox


class AssignmentBox(BaseBox):
    def __init__(self, word, value):
        self.word = word.getname()
        self.value = value.getvalue()

    def getvalue(self):
        return self.value

    def getident(self):
        return self.word


class TypeBox(BaseBox):
    def __init__(self, value):
        self.value = value.value

    def getvalue(self):
        return self.value


class IdentBox(BaseBox):
    def __init__(self, name):
        self.name = name.value

    def getname(self):
        return self.name


class AssignmentsBox(BaseBox):
    def __init__(self, assignments=None, assignment=None):
        self.assignments = assignments
        self.assignment = assignment

    def getlist(self):
        if self.assignments:
            return self.assignments.getlist() + [self.assignment]
        return []


class SendBox(BaseBox):
    def __init__(self, expr):
        print(expr)


class MultiplicationBox(BaseBox):
    def __init__(self, num, num1):
        self.num = int(num.value)
        self.num1 = int(num1.value)

    def get_value(self):
        return self.num * self.num1


class ExpressionBox(BaseBox):
    def __init__(self, word, value):
        self.word = word.getname()
        self.value = value.getvalue()

    def getvalue(self):
        return self.value

    def getident(self):
        return self.word


class ExpressionsBox(BaseBox):
    def __init__(self, word, value):
        self.word = word.getname()
        self.value = value.getvalue()

    def getvalue(self):
        return self.value

    def getident(self):
        return self.word