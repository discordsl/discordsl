from rply import LexerGenerator, ParserGenerator, Token
from parsing import *

lg = LexerGenerator()
lg.add("STRING", r"\".*\"")
lg.add("INTEGER", r"-?[0-9]+")
lg.add("ASSIGNMENT", r"to")
lg.add("VAR", r"set")
lg.add("OUT", r"(print|debug|log)")
lg.add("IDENTIFIER", r"[a-zA-Z_0-9]+")
lg.add("MINUS", r"-")
lg.add("ADDITION", r"\+")
lg.add("MULTIPLY", r"\*")
lg.add("DIVIDE", r"\/")
lg.ignore("\s")
lg.ignore("\n")
l = lg.build()
f = l.lex(open("day2.dsl", "r").read())

pg = ParserGenerator(["STRING", "INTEGER", "MINUS", "ADDITION", "MULTIPLY", "DIVIDE", "ASSIGNMENT",
                      "VAR", "IDENTIFIER", "OUT"],
                     cache_id="DiscordSL")


@pg.production("main : expr")
def main(p):
    return p[0]


@pg.production("num : INTEGER")
def assign_integer(p):
    return TypeBox(p[0])


@pg.production("ident : IDENTIFIER")
def assign_identifier(p):
    return IdentBox(p[0])


@pg.production("assignment : VAR ident ASSIGNMENT num ")
def assign_variable(p):
    print("ASSIGNED")
    box = AssignmentBox(p[1], p[3])
    print(box.getident(), box.getvalue())


@pg.production("assignments : assignment assignment")
def expr_assignment(p):
    box = AssignmentsBox(p[0], p[1])
    print(box.getlist())
    return box


@pg.production("exprs : expr exprs")
def expr_exprs(p):
    box = MultiplicationBox(p[0], p[1])
    print(box.get_value())
    return box


@pg.production("expr : INTEGER MULTIPLY INTEGER")
def expr_calc(p):
    return MultiplicationBox(p[0], p[2])

@pg.production("expr : OUT STRING")
def expr_send(p):
    SendBox(p[1].value)


parser = pg.build()
parser.parse(f)
